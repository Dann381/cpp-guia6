//Clase para crear una matriz bidimensional, agregar números a la matriz y mostrar sus datos
#include <stdlib.h>
#include <iostream>
using namespace std;
#include "Matrix.h"

//El constructor solo posee la variable "n" que es el tamaño de la matriz (Matriz[n][n])
Matrix::Matrix(){
    int n;
}

Matrix::Matrix(int n){
    this->n = n;
}

//Función utilizada para ingresar números a la matriz
int** Matrix::agregar_datos(int**matriz, int n){
    int dato;
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            //Si la fila es igual a la columna, la posición se llena automáticamente con un 0 para formar la diagonal
            if (fila == col){
                matriz[fila][col] = 0;
            }
            //De lo contrario, el usuario puede ingresar cualquier número
            else {
                cout << "Ingrese distancia del vector " << fila+1 << " al vector " << col+1 << ": ";
                cin >> dato;
                matriz[fila][col] = dato;
            }
        }
    }
    system("clear");
    return matriz;
}

//Función que recorre la matriz e imprime sus datos
void Matrix::mostrar_matrix(int** matriz, int n){
    cout << endl;
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            cout << matriz[fila][col] << " ";
        }
        cout << endl;
    }
}

//Función que inicializa la matriz con enteros (se forma la matriz solo con -1)
void Matrix::inicializar_matriz_enteros(int **matriz, int n){
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            matriz[fila][col] = -1;
        }
    }
}

//Esta función inicial iniciaiza la matriz con un caracter
void Matrix::inicializar_vector_caracter(string *vector){
    int n = this->n;
    int col;
  
    // recorre el vector.
    for (col=0; col<n; col++) {
        vector[col] = ' ';
    }
}

//Esta función funciona como un Main, ocupa algunas de las funciones anteriores para crear la matriz
int** Matrix::crear_matrix(){
    int n = this->n;
    string V[n];
    inicializar_vector_caracter(V);

    int **matriz;
    matriz = new int*[n];
    for(int i=0; i<n; i++){
        matriz[i] = new int[n];
    }

    inicializar_matriz_enteros(matriz, n);
    return matriz;
}

