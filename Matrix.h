#ifndef MATRIX_H
#define MATRIX_H

class Matrix{
    private:
        int n;
    public:
        Matrix();
        Matrix(int n);
        int** agregar_datos(int**matriz, int n);
        void mostrar_matrix(int** matriz, int n);
        void inicializar_matriz_enteros(int **matriz, int n);
        void inicializar_vector_caracter(string *vector);
        int** crear_matrix();
};
#endif