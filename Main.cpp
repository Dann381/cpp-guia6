//Main del programa que incluye el archivo .h de Matrix.cpp
#include <string>
#include <fstream>
#include <cctype>
#include <iostream>
#include <charconv>
using namespace std;
#include "Matrix.h"

//Función que recorre la matriz y forma un grafo de la matriz creada
void imprimir_grafo(int **matriz, char *vector, int n){
    int i, j;
    FILE *fp;
    
    //Se forma un archivo txt con la información necesaria de la matriz para luego crear el grafo
    fp = fopen("grafo.txt", "w");
    fprintf(fp, "%s\n", "digraph G {");
    fprintf(fp, "%s\n", "graph [rankdir=LR]");
    fprintf(fp, "%s\n", "node [style=filled fillcolor=yellow];");
  
    for (i=0; i<n; i++) {
        for (j=0; j<n; j++) {
            if (i != j) {
                if (matriz[i][j] > 0) {
                fprintf(fp, "%c%s%c [label=%d];\n", vector[i],"->", vector[j], matriz[i][j]);
                }
            }
        }
    }
    fprintf(fp, "%s\n", "}");
    fclose(fp);

    //Teniendo el archivo txt creado, se crea y se abre el grafo
    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
}

//Función que recorre la matriz y la imprime en conjunto con su posición
void imprimir_matriz(int **matriz, int n){
    int i, j;
    for (i=0; i<n; i++){
        for (j=0; j<n; j++){
            cout << "matriz[" << i << "," << j << "]: " << matriz[i][j] << " | ";
        }
        cout << endl;
    }
} 

//Función que recorre un vector de enteros e imprime su contenido
void imprimir_vector_entero(int *vector, int n){
    int i;
    for (i=0; i<n; i++){
        cout << "D[" << i << "]: " << vector[i] << " | "; 
    }
    cout << endl;
}

//Función que recorre un vector de chars e imprime su contenido
void imprimir_vector_caracter(char *vector, string nomVector, int n){
    int i;
    for (i=0; i<n; i++){
        cout << nomVector << "[" << i << "]: " << vector[i] << " | ";
    }
    cout << endl;
}

//Función que agrega nombres a los vectores según su código ASCII empezando por la "a"
void leer_nodos (char *vector, int n){
    int i;
    int inicio = 97;
    for (i=0; i<n; i++){
        vector[i] = inicio+i;
    }
}

//Función que añade los vértices al vector S
void agrega_vertice_a_S(char *S, char vertice, int n){
    int i;
    for (i=0; i<n; i++){
        if (S[i] == ' '){
            S[i] = vertice;
            return;
        }
    }  
}

//Función que busca un caracter en V
int buscar_indice_caracter(char *V, char caracter, int n){
    int i;
    
    for (i=0; i<n; i++){
        if (V[i] == caracter){
            return i;
        }
    }
    return i;
}

//Función que retorna si un caracter se encuentra en "vector" o no
int busca_caracter(char c, char *vector, int n){
    int j;
    for (j=0; j<n; j++){
        if (c == vector[j]){
            return true;
        }
    }
    return false;
}

//Función que elige vertice menor en VS[] según valores en D[], lo agrega a S[] y actualiza VS[]
int elegir_vertice(char *VS, int *D, char *V, int n){
    int i = 0;
    int menor = 0;
    int peso;
    int vertice; 
    while (VS[i] != ' '){
        peso = D[buscar_indice_caracter(V, VS[i], n)];
        if ((peso != -1) && (peso != 0)){
            if (i == 0){
                menor = peso;
                vertice = VS[i];
            } 
            else{
                if (peso < menor){
                    menor = peso;
                    vertice = VS[i];
                }
            }
        }
        i++;
    }
    cout << "vertice: " << char(vertice) << endl;
    return vertice;
}

//Función que calcula el mínimo entre 3 enteros entregados
int calcular_minimo(int dw, int dv, int mvw){
    int min = 0;
    if (dw == -1){
        if (dv != -1 && mvw != -1){
            min = dv + mvw;
        }
        else{
            min = -1;
        }
    }
    else{
        if (dv != -1 && mvw != -1){
            if (dw <= (dv + mvw)){
                min = dw;
            }
            else{
                min = (dv + mvw);
            }
        }
        else{
            min = dw;
        }
    }
    cout << "dw: " << dw;
    cout << " dv: " << dv;
    cout << " mvw: " << mvw;
    cout << " min: " << min << endl;
    return min;
}

//Dada la matriz M, se inicializa el vector D con sus valores correspondientes
void inicializar_vector_D(int *D, int **M, int n){
    int col;
    for (col=0; col<n; col++){
        D[col] = M[0][col];
    }
} 

//Inicializa vector con caracteres (los llena con " ")
void inicializar_vector_caracter(char *vector, int n){
    int col;
    for (col=0; col<n; col++){
        vector[col] = ' ';
    }
}

//Dado vectores V y S, actualiza el vector VS
void actualizar_VS(char *V, char *S, char *VS, int n){
    int j;
    int k = 0;
    inicializar_vector_caracter(VS, n);
    for (j=0; j<n; j++){
        if (busca_caracter(V[j], S, n) != true){
            VS[k] = V[j];
            k++;
        }
    }
}

//Actualiza pesos en D[] dada la matriz y los vectores D y V
void actualizar_pesos (int *D, char *VS, int **M, char *V, char v, int n){
    int i = 0;
    int indice_w, indice_v;
    cout << "\nActualiza pesos en D[]" << endl;
    
    indice_v = buscar_indice_caracter(V, v, n);
    while (VS[i] != ' '){
        if (VS[i] != v){
            indice_w = buscar_indice_caracter(V, VS[i], n);
            D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], M[indice_v][indice_w]);
        }
        i++;
    }
} 

//Aplica el algoritmo de Dijkstra usando las funciones anteriores, además de imprimir por terminal el
//contenido de los vectores, la matriz, y la distancia más hacia cada vector respecto el primer vector
void aplicar_dijkstra (char *V, char *S, char *VS, int *D, int **M, int n){
    int i;
    int v;
    inicializar_vector_D(D, M, n);
    cout << "---------Estados iniciales ---------------------------------------" << endl;
    imprimir_matriz(M, n);
    cout << endl;
    imprimir_vector_caracter(S, "S", n);
    imprimir_vector_caracter(VS, "VS", n);
    imprimir_vector_entero(D, n);
    cout << "------------------------------------------------------------------" << endl;
    cout << "> agrega primer valor V[0] a S[] y actualiza VS[]" << endl;
    agrega_vertice_a_S (S, V[0], n);
    imprimir_vector_caracter(S, "S", n);
    
    actualizar_VS (V, S, VS, n);
    imprimir_vector_caracter(VS, "VS", n);
    imprimir_vector_entero(D, n);

    for (i=1; i<n; i++){
        cout << "\n> elige vertice menor en VS[] según valores en D[]" << endl;
        cout << "> lo agrega a S[] y actualiza VS[]\n" << endl;
        v = elegir_vertice (VS, D, V, n);

        agrega_vertice_a_S (S, v, n);
        imprimir_vector_caracter(S, "S", n);

        actualizar_VS (V, S, VS, n);
        imprimir_vector_caracter(VS, "VS", n);

        actualizar_pesos(D, VS, M, V, v, n);
        imprimir_vector_entero(D, n);
    }
}

//Main del algoritmo
int main(int argc, char **argv){
    system("clear");
    //Convirte el parametro pasado por el usuario a un entero
    int n = atoi(argv[1]);
    //Si el tamaño es 2 o menor, termina el programa
    if (n < 3){
        cout << "El tamaño debe ser mayor a 2" << endl;
        exit(-1);
    }
    //Se crea la matriz y los vectores
    Matrix matrix = Matrix(n);
    int** M = matrix.crear_matrix();
    char V[n]; 
    char S[n]; 
    char VS[n];
    int D[n];
    //Se le piden datos al usuario para llenar la matriz e imprime su contenido
    matrix.agregar_datos(M, n);
    matrix.mostrar_matrix(M, n);
    //Inicia los vectores
    inicializar_vector_caracter(V, n);
    inicializar_vector_caracter(S, n);
    inicializar_vector_caracter(VS, n);
    
    leer_nodos(V, n);
    //Se aplica el algoritmo de Dijkstra
    aplicar_dijkstra (V, S, VS, D, M, n);
    //Se crea el grafo
    imprimir_grafo(M, V, n);
    return 0;
}


