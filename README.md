# Guía 6 - Unidad II

### Descripción
Este algoritmo consiste en la implementación del algoritmo de Dijkstra para encontrar las mínimas distancias entre vectores de una matriz respecto al vector inicial. Los datos de la matriz son ingresados por el usuario manualmente, a excepción de la diagonal de la matriz, la cuál se llena con 0 automáticamente para cumplir con el algoritmo de Dijkstra.  

### Ejecución
Luego de descargar los archivos contenidos en este repositorio, el usuario debe escribir vía terminal el comando "make" para compilar los archivos. Esto dejará un ejecutable llamado "dijkstra", el cual recibe un parámetro inicial de tipo entero luego de escribir el nombre del programa (ejemplo: ./dijkstra 4). El comando anterior iniciará el programa creando una matriz bidimensional de tamaño 4x4. El usuario debe ingresar un parametro mayor o igual a 3, de lo contrario, el programa terminará.

### Luego de ejecutar
Si se ha ejecutado el programa correctamente, se le pedirá al usuario las distancias de cada vector respecto al vector inicial. Las diagonales no se le piden al usuario ya que se llenan con 0 de forma predeterminada. Habiendo llenado la matriz con los datos a elección del usuario, el programa mostrará los datos de la matriz, los datos de los vectores creados, y por último mostrará las distancias menores entre el vector 1 y los demás vectores. **Cabe destacar que el número -1 en el programa significa infinito, lo que en términos de distancia significa que no existe un camino entre ambos vectores que poseen distancia -1.**

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
